#[=======================================================================[.rst:
QtTest
----------

This module defines functions to help use the Qt Test infrastructure.
The main function is :command:`qtest_discover_tests`.

.. command:: qtest_discover_tests

  Automatically add tests with CTest by querying the compiled test executable
  for available tests::

    qtest_discover_tests(target)

  ``qtest_discover_tests`` sets up a post-build command on the test executable
  that generates the list of tests by parsing the output from running the test
  with the ``-datatags`` argument. This ensures that the full list of
  tests, including instantiations of parameterized tests, is obtained.  Since
  test discovery occurs at build time, it is not necessary to re-run CMake when
  the list of tests changes.
#]=======================================================================]

function(qtest_discover_tests test_name TARGET)
  set(ctest_file_base "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}")
  set(ctest_include_file "${ctest_file_base}_tests.cmake")
  add_custom_command(TARGET ${TARGET}
    POST_BUILD
    COMMAND "${CMAKE_COMMAND}"
      -D "TEST_EXECUTABLE:FILEPATH=$<TARGET_FILE:${TARGET}>"
      -D "CTEST_FILE:FILEPATH=${ctest_include_file}"
      -P "${_QTTEST_DISCOVER_TESTS_SCRIPT}"
    BYPRODUCTS "${ctest_include_file}"
  )

  set_property(DIRECTORY APPEND PROPERTY TEST_INCLUDE_FILES
    "${ctest_include_file}")
endfunction()


###############################################################################

set(_QTTEST_DISCOVER_TESTS_SCRIPT
  ${CMAKE_CURRENT_LIST_DIR}/QtTestAddTests.cmake
)
