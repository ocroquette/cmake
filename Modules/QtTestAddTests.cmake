execute_process(
  COMMAND "${TEST_EXECUTABLE}" -datatags
  OUTPUT_VARIABLE output_variable
  ERROR_VARIABLE error_variable
  RESULT_VARIABLE result_variable
)
if(NOT "${result_variable}" EQUAL 0)
  string(REPLACE "\n" "\n    " output "${output}")
  message(FATAL_ERROR
    "Error running test executable.\n"
    "  Path: '${TEST_EXECUTABLE}'\n"
    "  Result: ${result_variable}\n"
    "  Output:\n"
    "    ${output_variable}\n"
    "  Error:\n"
    "    ${error_variable}\n"
  )
endif()

set(ctest_script_content)
string(REPLACE "\n" ";" output_lines "${output_variable}")
foreach(line ${output_lines})
  set(generated_name)
  if(line MATCHES "^([^ ]*) ([^ ]*) (.*)$")
    # Line contains a data set name, like in:
    #   test_qttestdemo myParameterizedTest data set name 1
    #   test_qttestdemo myParameterizedTest data set name 2
    set(generated_name "${CMAKE_MATCH_1}.${CMAKE_MATCH_2}:${CMAKE_MATCH_3}")
    string(APPEND ctest_script_content "add_test(\"${generated_name}\" \"${TEST_EXECUTABLE}\" \"${CMAKE_MATCH_2}:${CMAKE_MATCH_3}\")\n")
  elseif(line MATCHES "^([^ ]*) ([^ ]*)$")
    # Line doesn't contain a data set name, like in:
    #   test_qttestdemo myFirstTest
    #   test_qttestdemo mySecondTest
    set(generated_name "${CMAKE_MATCH_1}.${CMAKE_MATCH_2}")
    string(APPEND ctest_script_content "add_test(\"${generated_name}\" \"${TEST_EXECUTABLE}\" \"${CMAKE_MATCH_2}\")\n")
  endif()
  if(generated_name)
    # Make ctest aware of tests skipped with QSKIP()
    #    SKIP   : test_qttestdemo::mySkippedTest() Example of skipped test
    string(APPEND ctest_script_content "set_tests_properties(\"${generated_name}\" PROPERTIES SKIP_REGULAR_EXPRESSION \"SKIP   : \")\n")
  endif()
endforeach()

file(WRITE "${CTEST_FILE}" "${ctest_script_content}")
